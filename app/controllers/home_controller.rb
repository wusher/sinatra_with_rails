class HomeController < ApplicationController
  def index
    session[:init] ||= Time.now.to_i
    render json: session.as_json
  end

  def current
    binding.pry
    render json: current_user.as_json
  end
end
