# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
#run Rails.application
require 'encrypted_cookie'

require_relative './lib/cookie_monster'
require_relative './lib/my_app'

map '/' do
  #use Rack::Session::EncryptedCookie, :secret => "3A259469-E2C1-453B-BB4A-F9EF9B1543B13A259469-E2C1-453B-BB4A-F9EF9B1543B1"
  run Rails.application
end

map '/external' do
  #use CookieMonster




  #use Rack::Config do |env|
    #binding.pry
    #env[ActionDispatch::Cookies::SECRET_TOKEN] = Rails.application.config.secret_token
  #end
  #use ActionDispatch::Cookies
  #use ActionDispatch::Session::CookieStore, key: '_sinatra_with_rails_session'
  run MyApp.new
end

