require 'rubygems'
require 'sinatra/base'

class MyApp < Sinatra::Base  

  enable :sessions

  get "/" do  
    session[:init] ||= Time.now.to_i
    content_type :json
    session.to_json
  end  

  get "/info" do
    binding.pry
    content_type :json
    session.to_json
  end  

end

