require 'rubygems'
require 'sinatra'
require 'pry'
require 'rails'

require ::File.expand_path('../../config/environment',  __FILE__)

require_relative './my_app'

secret= "5fe413682c45e5dd33d55bf8f21f64b975802691d04aa65fa53e1dbeafad0258f3f9bd67ab46fce08184986de6710428607add8e2f97e6ab592bb405bc60d0e2"
key = "_sinatra_with_rails_session"



use Rack::Config do |env|
  
  env[ActionDispatch::Cookies::SECRET_TOKEN] = secret
  env[key] = secret
end

use ActionDispatch::Cookies
use ActionDispatch::Session::CookieStore, key: key


run MyApp
